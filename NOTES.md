- Find the file/folder with slug /document/psi/material-docs, from top to down.

- There must be two different commands, one for folder details (without childs) and listChild with a given folder.
	- DescribeFile( {"id" : "12341234" } // {"path" : "/path/to/file" }) to get information about a file. Build the breadcrumbs this. Revisions, etc.
	- ListFiles({"folderId" : "abc"}),

- Caching List Files.
- Fetch Details for a File every time.

// - ListModifiedFiles({"since": "5 mins ago"}); // Keeps cache in the UI. Not needed at this point.


1. Fetch a single folder by id
- Gets top level fields.
- Any field that points to an identity needs to return the identity(id, slug, firstName, lastName)
- Finds each children (Not deleted ones 'deletedAt === null').
//	- Per each child (document or folder), returns top level fields.
//	- If the child is a document, latest content.attributes version. (No revisions).


2. Same as 1) but with the sorting asc/desc by either the name or updatedAt or document.attributes.
 - Folder are sorted independtly from documents.


3. Add Pagination.


4. Add Filtering.

