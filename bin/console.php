#!/usr/bin/env php
<?php
define('APPPATH', realpath(__DIR__ . '/../') . '/');

require_once APPPATH . 'vendor/autoload.php';

use Symfony\Component\Console\Application;

$container = require APPPATH . 'config/container.php';

use Commands\CreateMockFilesCommand;
use Commands\DescribeFileCommand;
use Commands\ListFilesCommand;
use Commands\SearchFilesCommand;

$application = new Application();

$application->add($container->get(CreateMockFilesCommand::class));
$application->add($container->get(DescribeFileCommand::class));
$application->add($container->get(ListFilesCommand::class));
$application->add($container->get(SearchFilesCommand::class));

$application->run();
