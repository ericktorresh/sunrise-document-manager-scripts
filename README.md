# Set of commands for Sunrise Document Manager API

### Instructions

- Go to the project folder: `cd /path/to/project/folder`.
- Create config file: `cp .env.dist .env`.
- Run composer: `composer install`.

#### Services by Enviroment
- Create config/services_dev.yaml or config/services_prod.yaml with the following code:

```
imports:
  - { resource: services.yaml }
```

## Create Mock Data
- Run `php bin/console.php create-mock-files`

## Describe a File
- By Id `php bin/console.php describe-file --id=<id>`
- By Path `php bin/console.php describe-file --path=</path/to/file>`

## List Files per Folder
- By Id `php bin/console.php list-files --id=<id>`
- By Path `php bin/console.php list-files --path=</path/to/file>`
