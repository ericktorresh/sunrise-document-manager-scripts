// Complex query: get folder and its children in one query
db.getCollection('files').aggregate([
    {
        $match: { _id: ObjectId("5c423e783f968a82147cde54") }
    },
    {
        $sort: { createBy: -1, "identity.firstName": 1 }
    },
    {
        $limit:  10
    },
    {
        $skip: 0
    },
    {
        $lookup : {
             from: "file_types",
             localField: "typeId",
             foreignField: "_id",
             as: "fileTypes"
        }
    },
    {
        $addFields: { "fileType": { $arrayElemAt: [ "$fileTypes", 0 ] } },
    },
    {
        $lookup : {
            from: "files",
            let: { localId: "$_id" },
            pipeline: [
              { $match:
                 { $expr:
                    { $and:
                       [
                         { $eq: [ "$parentId",  "$$localId" ] },
                         { $eq: [ "$deletedAt", null ] }
                       ]
                    }
                 }
              },
              {
                    $lookup : {
                         from: "identities",
                         localField: "createdBy",
                         foreignField: "_id",
                         as: "identities"
                    }
                },
                {
                    $addFields: { "identity": { $arrayElemAt: [ "$identities", 0 ] } }
                },
            ],
            as: "children"
        }
    },
    {
        $project: {
            typeId: 0,
            fileTypes: 0,
            "children.typeId": 0,
            "children.version": 0,
            "children.deletedAt": 0,
            "children.deletedBy": 0,
            "children.identities": 0,
        }
    },
])

// Find children
db.getCollection('files').aggregate([
    {
        $match: { 'parentId': ObjectId("5c478fa83f968af50a6a8734"), 'deletedAt': null }
    },
    {
        $lookup : {
             from: "file_types",
             localField: "typeId",
             foreignField: "_id",
             as: "fileTypes"
        }
    },
    {
        $addFields: { "fileType": { $arrayElemAt: [ "$fileTypes", 0 ] } },
    },
    {
        $sort: { "fileType.isFolder": -1, "name": 1 },
    },
    {
        $limit: 10,
    },
    {
        $skip: 0,
    },
    {
        $lookup: {
            from: 'identities',
            localField: 'createdBy',
            foreignField: '_id',
            as: 'identities'
        }
    },
    {
        $lookup: {
            from: 'identities',
            localField: 'updatedBy',
            foreignField: '_id',
            as: 'identities2'
        }
    },
    {
        $addFields: {
            "createdBy": { $arrayElemAt: [ "$identities", 0 ] },
            "updatedBy": { $arrayElemAt: [ "$identities2", 0 ] },
        },
    },
    {
        $project: {
            typeId: 0,
            fileTypes: 0,
            identities: 0,
            identities2: 0,
        }
    },
])

// Find File: the same of above but using _id instead of parentId

// Search info
db.getCollection('files').createIndex({ name: "text" })

db.getCollection('files').aggregate([
    {
        $match: {
           $text: {
              $search: "File",
              $caseSensitive: false,
            }
        }
    },
    {
        $sort: { createBy: -1, "identity.firstName": 1 }
    },
    {
        $limit:  10
    },
    {
        $skip: 0
    },
    {
        $lookup: {
            from: 'identities',
            localField: 'createdBy',
            foreignField: '_id',
            as: 'identities'
        }
    },
    {
        $lookup : {
             from: "file_types",
             localField: "typeId",
             foreignField: "_id",
             as: "fileTypes"
        }
    },
    {
        $addFields: {
            "identity": { $arrayElemAt: [ "$identities", 0 ] },
            "fileType": { $arrayElemAt: [ "$fileTypes", 0 ] }
        },
    },
    {
        $project: {
            typeId: 0,
            fileTypes: 0,
            identities: 0,
        }
    },
])


// Find related Files
db.getCollection('files').aggregate([
    {
        $match: {
            'slug': 'folder-1',
            'parentId': null,
            'deletedAt': null,
        }
    },
    {
       $unwind: {
          "path": "$content.relatedFiles.file",
          "preserveNullAndEmptyArrays": true
      }
    },
    {
        $lookup: {
            from: 'files',
            localField: 'content.relatedFiles.fileId',
            foreignField: '_id',
            as: 'content.relatedFiles.file'
        }
    },
    {
      $unwind:"$content.relatedFiles.file"
    },
    {
        "$group": {
            "_id": "$_id",
            "attributes": { $first: "$content.attributes" },
            "attachments": { $first: "$content.attachments" },
            "relatedFiles": { "$push": "$content.relatedFiles" },
            "file":{ "$first":"$$ROOT" },
        }
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [ "$file", "$$ROOT" ] } }
    },
    {
        $project: {
            "parentId": 1,
            "name": 1,
            "slug": 1,
            "createdBy": 1,
            "createdAt": 1,
            "updatedBy": 1,
            "updatedAt": 1,
            "type": 1,
            "version": 1,
            "content.relatedFiles": "$relatedFiles",
            "content.attributes": "$attributes",
            "content.attachments": "$attachments",
        }
    }
])



// Get all folder of the root

db.getCollection('files').aggregate([
    {
        $match: {
            'parentId': null,
            'deletedAt': null,
        }
    },
    {
        $skip: 0
    },
    {
        $limit:  1
    },
    {
        $lookup: {
            from: 'files',
            let: { localId: "$_id" },
            pipeline: [
              {
                  $match: {
                    $expr: { $and:
                       [
                         { $eq: [ "$parentId",  "$$localId" ] },
                         { $eq: [ "$deletedAt", null ] }
                       ]
                    }
                  }
              },
            ],
            as: "children"
        }
    }
])

// Related files

db.getCollection('files').aggregate([
    {
        $match: {
            '_id': ObjectId("5c4f5a813f968a247c33f3d6"),
            'deletedAt': null,
        }
    },
    {
      $unwind: {
          "path": "$content.relatedFiles",
          "preserveNullAndEmptyArrays": true
      },
    },
    {
        $lookup: {
            from: 'files',
            localField: 'content.relatedFiles.fileId',
            foreignField: '_id',
            as: 'file'
        }
    },
    {
      $unwind:"$file"
    },
    {
        $project: {
            "description": "$content.relatedFiles.description",
            "linkedBy": "$content.relatedFiles.linkedBy",
            "linkedAt": "$content.relatedFiles.linkedAt",
            "file._id": 1,
            "file.parentId": 1,
            "file.name": 1,
            "file.slug": 1,
        }
    }
])
