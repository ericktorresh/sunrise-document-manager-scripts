<?php
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(realpath(APPPATH . '.env'));

$container = new ContainerBuilder();

$basePath = APPPATH . 'config/';

$loader = new YamlFileLoader($container, new FileLocator($basePath));

$environment = getenv('ENVIRONMENT');

switch (true) {
	case (
		$environment === 'dev' &&
		file_exists($basePath . 'services_dev.yaml')
	):
		$loader->load('services_dev.yaml');
		break;
	case (
		$environment === 'prod' &&
		file_exists($basePath . 'services_prod.yaml')
	):
		$loader->load('services_prod.yaml');
		break;
	default:
		$loader->load('services.yaml');
		break;
}

$container->compile(true);

return $container;
