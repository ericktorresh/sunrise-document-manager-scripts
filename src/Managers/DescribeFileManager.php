<?php
declare(strict_types = 1);
namespace Managers;

use InvalidArgumentException;
use MongoDB\Client;
use MongoDB\BSON\ObjectId;

class DescribeFileManager extends FileManager
{
	public function byPath(string $path) : array
	{
		$path = trim($path, '/');

		if (strlen($path) === 0)
		{
			throw new InvalidArgumentException('Invalid path. It should contain at least a root.');
		}

		$file = $this->findFileByPath($path, null);

		if ($file === null)
		{
			throw new InvalidArgumentException('File not found!');
		}

		return [
			'file' => $file,
			'breadcrumbs' => $this->builbBreadcrumbs($file),
			'file_type' => $this->getFileType($file),
			'revisions' => $this->getRevisions($file),
		];
	}

	public function byId(string $id) : array
	{
		$file = $this->findFile(['_id' => new ObjectId($id)]);

		if ($file === null)
		{
			throw new InvalidArgumentException('File not found!');
		}

		return [
			'file' => $file,
			'breadcrumbs' => $this->builbBreadcrumbs($file),
			'file_type' => $this->getFileType($file),
			'revisions' => $this->getRevisions($file),
		];
	}
}
