<?php
declare(strict_types = 1);
namespace Managers;

use InvalidArgumentException;
use MongoDB\Client;
use MongoDB\BSON\ObjectId;
use MongoDB\Model\BSONDocument;

class FileManager
{
	protected $client;
	protected $database;

	public function __construct(Client $client)
	{
		$this->client = $client;
		$this->database = $this->client->document_manager_test;
	}

	public function builbBreadcrumbs(BSONDocument $file) : array
	{
		$breadcrumbs = $this->findParents($file, [
			'_id' => 1,
			'name' => 1,
			'slug' => 1
		]);

		return $breadcrumbs;
	}

	public function findParents(BSONDocument $file, array $project = [], array $parents = []) : array
	{
		$project = $project + ['parentId' => 1];

		if ($file->parentId === null)
		{
			return $parents;
		}

		$parent = $this->findFile(['_id' => $file->parentId], $project);

		if ($parent === null)
		{
			return $parents;
		}

		array_unshift($parents, $parent);

		return $this->findParents($parent, $project, $parents);
	}

	public function findFileByPath(string $path, ?ObjectId $parentId) : ?BSONDocument
	{
		$slugs = explode('/', $path);

		$slug = array_shift($slugs);

		$file = $this->findFile(['slug' => $slug, 'parentId' => $parentId]);

		if (count($slugs) === 0 || $file === null)
		{
			return $file;
		}

		return $this->findFileByPath(implode('/', $slugs), $file->_id);
	}

	public function findFile(array $match = [], array $project = [])
	{
		$project = $project === [] ? ['identities' => 0] : $project;

		$files = $this->database->files->aggregate([
			[
				'$match' => $match + ['deletedAt' => null]
			],
			[
				'$lookup' => [
					'from' => 'identities',
					'localField' => 'createdBy',
					'foreignField' => '_id',
					'as' => 'identities'
				]
			],
			[
				'$addFields' => [
					"createdBy" => [
						'$arrayElemAt'=> [ '$identities', 0 ]
					]
				]
			],
			[
				'$project' => $project,
			],
		]);

		return $files->toArray()[0] ?? null;
	}

	public function findFiles(
		array $match,
		int $skip = 0,
		int $limit = 10,
		array $sort = []
	) : array
	{
		$sort = $sort === [] ? ['name' => 1] : $sort;

		$files = $this->database->files->aggregate([
			[
				'$match' => $match + ['deletedAt' => null]
			],
			[
				'$sort' => ['type.isFolder' => -1] + $sort
			],
			[
				'$limit' => $limit
			],
			[
				'$skip' => $skip
			],
			[
				'$lookup' => [
					'from' => 'identities',
					'localField' => 'createdBy',
					'foreignField' => '_id',
					'as' => 'identities'
				]
			],
			[
				'$lookup' => [
					'from' => 'identities',
					'localField' => 'updatedBy',
					'foreignField' => '_id',
					'as' => 'identities2'
				]
			],
			[
				'$addFields' => [
					'createdBy' => ['$arrayElemAt'=> ['$identities', 0]],
					'updatedBy' => ['$arrayElemAt'=> ['$identities2', 0]]
				]
			],
			[
				'$project' => [
					'identities' => 0,
					'identities2' => 0,
					'file_types' => 0
				],
			],
		]);

		return $files->toArray();
	}

	public function getRevisions(BSONDocument $file) : array
	{
		$revisions = $this->database->file_revisions->aggregate([
			[
				'$match' => [
					'fileId' => $file->_id,
				]
			],
			[
				'$lookup' => [
					'from' => 'identities',
					'localField' => 'createdBy',
					'foreignField' => '_id',
					'as' => 'identities'
				]
			],
			[
				'$addFields' => [
					"identity" => [
						'$arrayElemAt'=> [ '$identities', 0 ]
					]
				]
			],
			[
				'$project' => [ 'identities' => 0 ],
			],
		]);

		return $revisions->toArray();
	}

	public function getFileType(BSONDocument $file): BSONDocument
	{
		$fileType = $this->database->file_types->findOne([
			'_id' => $file->type->id
		]);

		return $fileType;
	}
}
