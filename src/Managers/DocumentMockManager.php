<?php
declare(strict_types = 1);
namespace Managers;

use MongoDB\Client;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class DocumentMockManager
{
	public function __construct(Client $client)
	{
		$this->client = $client;
		$this->database = $this->client->document_manager_test;
		$this->createdFolderCount = 0;
		$this->createdFileCount = 0;
		$this->fileCountIndex = 0;
		$this->folderCountIndex = 0;
		$this->createdFileIds = [];

		$jsonContent = file_get_contents(APPPATH . 'etc/mockData.json');
		$this->data = json_decode($jsonContent, true);
	}

	private function buildFileTypes()
	{
		$attributes = $this->data['fileAttributes'];

		foreach($this->data['fileTypes'] as $id => $type)
		{
			$data = $type;

			if ($id !== 'root')
			{
				$data = $data + ['attributes' => $attributes];
			}

			$insertResult = $this->database->file_types->insertOne($data);

			$this->data['fileTypes'][$id]['_id'] = $insertResult->getInsertedId();
		}

		foreach($this->data['fileTypes'] as $id => $type)
		{
			if (isset($type['allowedFileTypes']) && is_array($type['allowedFileTypes']))
			{
				$allowedFilesTypeIds = array_map(function($fileTypeName) {
					return $this->data['fileTypes'][$fileTypeName]['_id'];
				}, $type['allowedFileTypes']);

				$this->database->file_types->updateOne(
					['_id' => $type['_id']],
					[
						'$set' => [
							'allowedFileTypes' => $allowedFilesTypeIds,
						]
					]
				);
			}
		}
	}

	private function buildIdentities()
	{
		$baseRoles = $this->data['roles'];

		foreach($this->data['identities'] as $key => $identity)
		{
			$roles = $this->pickRandomFromArray($key, $baseRoles, 1, 2);

			$data = $identity + [
				'slug' => $this->slugify($identity['firstName'] . ' ' . $identity['lastName']),
				'roles' => $roles,
			];

			$insertResult = $this->database->identities->insertOne($data);

			$this->data['identities'][$key]['_id'] = $insertResult->getInsertedId();
		}
	}

	private function inserFolders($folders, $parentId = null)
	{
		if ($folders === [])
		{
			return;
		}

		foreach ($folders as $folder)
		{
			$folders = $folder['folders'] ?? [];
			$files = $folder['files'] ?? [];

			$folderCopy = $folder;
			unset($folderCopy['folders'], $folderCopy['files']);

			$insertResult = $this->database->files->insertOne(
				['parentId' => $parentId] + $folderCopy
			);

			$nextParentId = $insertResult->getInsertedId();

			foreach ($files as $file)
			{
				$insertResult = $this->database->files->insertOne(['parentId' => $nextParentId] + $file);
				$this->createdFileIds[] = $insertResult->getInsertedId();
				$this->createdFileCount++;
			}

			$folders = $folder['folders'] ?? [];

			$this->inserFolders($folders, $nextParentId);
		}
	}

	private function generateRootFolder()
	{
		$folders = [];

		$folderType = $this->data['fileTypes']['folder'];

		foreach($this->data['rootFolders'] as $key => $folder)
		{
			$time = $this->getRandomDateTime();

			$identity = $this->pickRandomFromArray($key, $this->data['identities'], 1, 1);

			srand($key);

			$folders[] = [
				'name' => $folder['name'],
				'slug' => $this->slugify($folder['name']),
				'createdBy' => $identity['_id'],
				'createdAt' => $time,
				'updatedBy' => $identity['_id'],
				'updatedAt' => $time,
				'deletedBy' => null,
				'deletedAt' => null,
				'type' => [
					'id' => $folderType['_id'],
					'name' => $folderType['name'],
					'isFolder' => true,
				],
				'folders' => $this->generateFolders(rand(1, 3)),
				'files' => $this->generateFiles(rand(1, 3)),
			];
		}

		$rootFolderType = $this->data['fileTypes']['root'];

		$time = $this->getRandomDateTime();
		$identity = $this->pickRandomFromArray($key, $this->data['identities'], 1, 1);

		return [
			[
				'name' => 'root',
				'slug' => null,
				'createdBy' => $identity['_id'],
				'createdAt' => $time,
				'updatedBy' => $identity['_id'],
				'updatedAt' => $time,
				'deletedBy' => null,
				'deletedAt' => null,
				'type' => [
					'id' => $rootFolderType['_id'],
					'name' => $rootFolderType['name'],
					'isFolder' => true,
				],
				'folders' => $folders,
				'files' => [],
			]
		];
	}

	private function generateFolders($count, $depth = 1)
	{
		$folders = [];

		$folderType = $this->data['fileTypes']['folder'];

		foreach(range(0, $count - 1) as $index)
		{
			if ($this->maxDepth <= $depth)
			{
				continue;
			}

			$identity = $this->pickRandomFromArray($index, $this->data['identities'], 1, 1);

			$time = $this->getRandomDateTime();

			$this->folderCountIndex++;

			$name = 'Folder ' . str_pad((string) $this->folderCountIndex, 3, '0', STR_PAD_LEFT);

			$folders[] = [
				'name' => $name,
				'slug' => $this->slugify($name),
				'createdBy' => $identity['_id'],
				'createdAt' => $time,
				'updatedBy' => $identity['_id'],
				'updatedAt' => $time,
				'deletedBy' => null,
				'deletedAt' => null,
				'type' => [
					'id' => $folderType['_id'],
					'name' => $folderType['name'],
					'isFolder' => true,
				],
				'folders' => $this->generateFolders(rand(1, 3), $depth + 1),
				'files' => $this->generateFiles(rand(1, 3)),
			];
		}

		return $folders;
	}

	private function generateFiles($count)
	{
		return array_map(function($index)
		{
			$fileType = $this->data['fileTypes']['document'];

			$time = $this->getRandomDateTime();

			$this->fileCountIndex++;

			$identity = $this->pickRandomFromArray($index, $this->data['identities'], 1, 1);

			$name = 'File ' . str_pad((string) $this->fileCountIndex, 3, '0', STR_PAD_LEFT);

			$attributesValues = [];

			foreach($this->data['fileAttributes'] as $key => $attribute)
			{
				$value = null;

				// Ignore top level fields
				if (
					in_array($key, [
						'name',
						'createdAt',
						'createdBy',
						'updatedAt',
						'updatedBy',
						'deletedAt',
						'deletedBy'
					])
				)
				{
					continue;
				}

				if ($attribute['type'] === 'string' || $attribute['type'] === 'text')
				{
					$value = 'Value for file #' . $key;
				}
				else if ($attribute['type'] === 'datetime')
				{
					$value = $this->getRandomDateTime();
				}
				else if ($attribute['type'] === 'object')
				{
					$value = [];
				}

				$attributesValues[$key] = [
					'value' => $value,
					'updatedBy' => $identity['_id'],
					'updatedAt' => $time,
				];
			}

			srand($index);
			$numAttachments = rand(0, 5);

			$attachments = array_map(function($index) {
				$identity = $this->pickRandomFromArray($index, $this->data['identities'], 1, 1);
				$time = $this->getRandomDateTime();

				return [
					'id' => 'attachment-' . $index . '-' . $this->fileCountIndex,
					'name' => 'Attachment ' . $this->fileCountIndex,
					'mimeType' => 'application/pdf',
					'fileSize' => rand(5000, 1000000),
					'uploadedBy' => $identity['_id'],
					'uploadedAt' => $time,
					'url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfH_PMFJbQvnPf9A5mYNaP2K1tsuPGaQw2mnFAHKTNtTijDFSggw',
				];
			}, range(0, $numAttachments));

			return [
				'name' => $name,
				'slug' => $this->slugify($name),
				'createdBy' => $identity['_id'],
				'createdAt' => $time,
				'updatedBy' => $identity['_id'],
				'updatedAt' => $time,
				'deletedBy' => null,
				'deletedAt' => null,
				'type' => [
					'id' => $fileType['_id'],
					'name' => $fileType['name'],
					'isFolder' => $fileType['isFolder'],
				],
				'version' => 1,
				'content' => [
					'attributes' => $attributesValues,
					'relatedFiles' => [],
					'attachments' => $attachments
				]
			];
		}, range(0, $count - 1));
	}

	private function linkFiles()
	{
		srand();
		$randNumber = rand(5, (integer) ($this->fileCountIndex / 2));
		$linked = [];

		foreach (range(0, $randNumber) as $number)
		{
			srand();
			// Get random index
			$index = rand(0, $this->fileCountIndex - 1);
			$index2 = rand(0, $this->fileCountIndex - 1);

			if (
				isset(
					$this->createdFileIds[$index],
					$this->createdFileIds[$index2]
				) &&
				$index !== $index2 &&
				(
					isset($linked[$index]) === false ||
					$linked[$index] !== $index2
				)
			)
			{
				$identity = $this->pickRandomFromArray($index + $index2, $this->data['identities'], 1, 1);
				$time = $this->getRandomDateTime();

				$file1 = $this->database->files->findOne(['_id' => $this->createdFileIds[$index]]);
				$file2 = $this->database->files->findOne(['_id' => $this->createdFileIds[$index2]]);

				$this->database->files->updateOne(
					['_id' => $file1->_id],
					[
						'$set' => [
							'updatedAt' => $time,
						],
						'$push' => [
							'content.relatedFiles' => [
								'fileId' => $file2->_id,
								'description' => '',
								'linkedBy' => $identity['_id'],
								'linkedAt' => $time,
							]
						]
					]
				);

				$linked[$index] = $index2;
			}
		}
	}

	private function generateRevisions() {
		$randNumber = rand(5, $this->fileCountIndex - 1);

		foreach (range(0, $randNumber) as $number)
		{
			// Get random index
			$index = rand(0, $this->fileCountIndex - 1);

			if (isset($this->createdFileIds[$index]))
			{
				$identity = $this->pickRandomFromArray($index, $this->data['identities'], 1, 1);
				$time = $this->getRandomDateTime();

				$file = $this->database->files->findOne(['_id' => $this->createdFileIds[$index]]);

				$this->database->file_revisions->insertOne([
					'fileId' => $file->_id,
					'version' => $file->version,
					'createdBy' => $identity['_id'],
					'createdAt' => $time,
					'content' => $file->content,
				]);

				$newVersion = $file->version + 1;

				$this->database->files->updateOne(
					['_id' => $file->_id],
					['$set' => [
						'version' => $newVersion,
						'updatedBy' => $identity['_id'],
						'updatedAt' => $time,
						'content.attributes.notes' => [
							'value' => '(Edited) (R' . $newVersion . ') ' . $file->content->attributes->notes->value,
							'updatedBy' => $identity['_id'],
							'updatedAt' => $time,
						]
					]]
				);
			}
		}
	}

	public function createMocks(
		$initalFolderCount = 4,
		$reset = true,
		$maxDepth = 5
	)
	{
		if ($reset === true)
		{
			$this->database->drop();

			$this->database->files->createIndex(['name' => 'text']);
		}

		$this->maxDepth = $maxDepth;

		$this->buildFileTypes();
		$this->buildIdentities();

		$folders = $this->generateRootFolder($initalFolderCount);

		$session = $this->client->startSession();
		$session->startTransaction();

		try
		{
			$this->inserFolders($folders);

			// Associated random files
			$this->linkFiles();
			// Generates some revision
			$this->generateRevisions();

			$session->commitTransaction();
		}
		catch (Exception $e)
		{
			$session->abortTransaction();
			throw $e;
		}

		return [
			'tree' => $folders = [],
			'total' => $this->createdFileCount,
		];
	}

	public function slugify($text)
	{
		$text = strtolower(trim($text));
		$text = strtr($text, ['&' => 'and', ' ' => '-', '_' => '-', ':' => '-']);
		$text = preg_replace('/[^a-z0-9-]+/', '', $text);
		$text = preg_replace('/-+/', '-', $text);
		$text = trim($text, '-');

		return $text;
	}

	private function getRandomDateTime()
	{
		return new UTCDateTime(strtotime('+' . mt_rand(0,30) . ' days')  * 1000);
	}

	private function pickRandomFromArray(
		int $seed,
		array $sourceArray,
		int $minNumElements = 1,
		int $maxNumElements = 1
	)
	{
		srand($seed);

		$numElements = rand($minNumElements, $maxNumElements);

		$chosenElements = array_reduce(
			range(1, $numElements - 1),
			function($carry, $index) use ($seed, &$sourceArray) {
				srand($index + $seed);

				$sIndex = rand(0, count($sourceArray) - 1);

				$value = $sourceArray[$sIndex];

				$carry[] = $value;

				unset($sourceArray[$sIndex]);

				$sourceArray = array_values($sourceArray);

				return $carry;
			},
			[]
		);

		return ($minNumElements === 1 && $maxNumElements === 1) ?
			$chosenElements[0] :
			$chosenElements;
	}
}
