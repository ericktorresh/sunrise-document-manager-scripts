<?php
declare(strict_types = 1);
namespace Managers;

use InvalidArgumentException;
use MongoDB\Client;
use MongoDB\BSON\ObjectId;

class ListFilesManager extends FileManager
{
	public function byPath(string $path, int $skip, int $limit) : array
	{
		$path = trim($path, '/');

		if (strlen($path) === 0)
		{
			throw new InvalidArgumentException('Invalid path. It should contain at least a root.');
		}

		$file = $this->findFileByPath($path, null);

		if ($file === null)
		{
			throw new InvalidArgumentException('File not found!');
		}

		if ($file->type->isFolder === false)
		{
			throw new InvalidArgumentException('Only folders allowed!');
		}

		$children = $this->findFiles(
			['parentId' => $file->_id],
			$skip,
			$limit
		);

		return [
			'file' => $file,
			'breadcrumbs' => $this->builbBreadcrumbs($file),
			'children' => $children
		];
	}

	public function byId(string $id, int $skip, int $limit) : array
	{
		$file = $this->findFile(['_id' => new ObjectId($id)]);

		if ($file === null)
		{
			throw new InvalidArgumentException('File not found!');
		}

		if ($file->type->isFolder === false)
		{
			throw new InvalidArgumentException('Only folders allowed!');
		}

		$children = $this->findFiles(
			['parendId' => $file->_id],
			$skip,
			$limit
		);

		return [
			'file' => $file,
			'breadcrumbs' => $this->builbBreadcrumbs($file),
			'children' => $children
		];
	}
}
