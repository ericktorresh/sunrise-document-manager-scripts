<?php
namespace Commands;

use InvalidArgumentException;
use MongoDB\Driver\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Managers\ListFilesManager;

class ListFilesCommand extends Command
{
	protected static $defaultName = 'list-files';

	private $listFileManager;

	public function __construct(ListFilesManager $listFileManager)
	{
		$this->listFileManager = $listFileManager;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setDescription('Get children for a given folder.');

		$this->setDefinition(
			new InputDefinition([
				new InputOption('path', 'p', InputOption::VALUE_OPTIONAL),
				new InputOption('id', 'i', InputOption::VALUE_OPTIONAL),
				new InputOption('skip', 's', InputOption::VALUE_OPTIONAL, '', 0),
				new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, '', 10),
			])
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$fileId = $input->getOption('id');
			$filePath = $input->getOption('path');
			$skip = $input->getOption('skip');
			$limit = $input->getOption('limit');

			if (!$fileId && !$filePath)
			{
				throw new InvalidArgumentException('id or path is required');
			}

			$output->writeln([
				'Listing Files',
				'===============',
				'',
			]);

			$output->writeln([
				'fileId: ' . $fileId,
				'Path: ' . $filePath,
				'',
			]);

			if ($fileId)
			{
				list(
					'file' => $file,
					'breadcrumbs' => $breadcrumbs,
					'children' => $children,
				) = $this->listFileManager->byId($fileId, $skip, $limit);
			}
			else
			{
				list(
					'file' => $file,
					'breadcrumbs' => $breadcrumbs,
					'children' => $children,
				) = $this->listFileManager->byPath($filePath, $skip, $limit);
			}

			$output->writeln('Found file: ' . $file->name);
			$output->writeln('Files count: ' . count($children));
			$output->write('Breadcrumbs: ');

			while (($part = array_shift($breadcrumbs)))
			{
				$output->write($part->name . ' > ');
			}

			$output->write($file->name);

			$output->writeln([
				'',
				'',
				'=========',
				'Children',
				'=========',
			]);

			while (($child = array_shift($children)))
			{
				$output->writeln('|-' . $child->name);
			}

			$output->writeln('');
		}
		catch (Exception\InvalidArgumentException $e)
		{
			throw new \Exception('Oop! Looks like the id is invalid. ' . $e->getMessage());
		}
	}
}
