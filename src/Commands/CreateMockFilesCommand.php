<?php
namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Managers\DocumentMockManager;

class CreateMockFilesCommand extends Command
{
	protected static $defaultName = 'create-mock-files';

	private $documentManager;

	public function __construct(DocumentMockManager $documentManager)
	{
		$this->documentManager = $documentManager;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setDescription('Fill the database with random files and folder.');

		$this->setDefinition(
			new InputDefinition([
				new InputOption('initial', 'i', InputOption::VALUE_OPTIONAL, 'Initial numbers of folders', 3),
				new InputOption('keep', 'k', InputOption::VALUE_NONE, 'Don\'t Reset database'),
				new InputOption('tree', 't', InputOption::VALUE_NONE, 'Show Tree'),
				new InputOption('maxDepth', 'm', InputOption::VALUE_OPTIONAL, 'Max folder Depth ', 5),
			])
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$initial = $input->getOption('initial');
		$keepData = $input->getOption('keep');
		$maxDepth = $input->getOption('maxDepth');
		$showTree = $input->getOption('tree');

		$output->writeln([
			'Creating Files and Folder into Database',
			'=======================================',
		]);

		try
		{
			[
				'tree' => $folders,
				'total' => $total
			] = $this->documentManager->createMocks(
				$initial,
				!$keepData,
				$maxDepth
			);

			$renderTree = function($folders, $depth = '-') use ($output, &$renderTree)
			{
				foreach ($folders as $folder)
				{
					$output->writeln($depth . $folder['name']);

					foreach ($folder['files'] as $file)
					{
						$output->writeln($depth . $file['name']);
					}

					$renderTree($folder['folders'], $depth . '-');
				}
			};

			if ($showTree)
			{
				$renderTree($folders);
			}

			$output->writeln('Total files created: ' . $total);
		}
		catch (Exception $e)
		{
			$output->writeln('Oop! Something went wrong. ' . $e->getMessage());
		}
	}
}
