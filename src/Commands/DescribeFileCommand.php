<?php
namespace Commands;

use InvalidArgumentException;
use MongoDB\Driver\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Managers\DescribeFileManager;

class DescribeFileCommand extends Command
{
	protected static $defaultName = 'describe-file';

	private $describeFileManager;

	public function __construct(DescribeFileManager $describeFileManager)
	{
		$this->describeFileManager = $describeFileManager;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setDescription('Get detailed information about a file.');

		$this->setDefinition(
			new InputDefinition([
				new InputOption('path', 'p', InputOption::VALUE_OPTIONAL),
				new InputOption('id', 'i', InputOption::VALUE_OPTIONAL),
			])
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$fileId = $input->getOption('id');
			$filePath = $input->getOption('path');

			if (!$fileId && !$filePath)
			{
				throw new InvalidArgumentException('id or path is required');
			}

			$output->writeln([
				'Describing File ',
				'===============',
				'',
			]);

			$output->writeln([
				'fileId: ' . $fileId,
				'Path: ' . $filePath,
				'',
			]);

			if ($fileId)
			{
				list(
					'file' => $file,
					'breadcrumbs' => $breadcrumbs,
					'revisions' => $revisions,
					'file_type' => $fileType,
				) = $this->describeFileManager->byId($fileId);
			}
			else
			{
				list(
					'file' => $file,
					'breadcrumbs' => $breadcrumbs,
					'revisions' => $revisions,
					'file_type' => $fileType,
				) = $this->describeFileManager->byPath($filePath);
			}

			$output->writeln('Found file: ' . $file->name);
			$output->write('Breadcrumbs: ');

			while (($part = array_shift($breadcrumbs)))
			{
				$output->write($part->name . ' > ');
			}

			$output->write($file->name);

			$output->writeln('');
		}
		catch (Exception\InvalidArgumentException $e)
		{
			throw new \Exception('Oop! Looks like the id is invalid. ' . $e->getMessage());
		}
	}
}
