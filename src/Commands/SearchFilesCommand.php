<?php
namespace Commands;

use InvalidArgumentException;
use MongoDB\Driver\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Managers\FileManager;

class SearchFilesCommand extends Command
{
	protected static $defaultName = 'search-files';

	private $fileManager;

	public function __construct(FileManager $fileManager)
	{
		$this->fileManager = $fileManager;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setDescription('Find folders and files.');

		$this->setDefinition(
			new InputDefinition([
				new InputOption('search', 's', InputOption::VALUE_REQUIRED),
				new InputOption('skip', 'o', InputOption::VALUE_OPTIONAL, '', 0),
				new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, '', 10),
			])
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$query = $input->getOption('search');
			$skip = $input->getOption('skip');
			$limit = $input->getOption('limit');

			$output->writeln([
				'Search Files',
				'===============',
				'',
			]);

			$output->writeln([
				'query: ' . $query,
				'',
			]);

			$children = $this->fileManager->findFiles(
				[
					'$text' => [
						'$search' => $query,
						'$caseSensitive' => false,
					]
				],
				$skip,
				$limit
			);

			$output->writeln('Files count: ' . count($children));

			$output->writeln([
				'',
				'',
				'=========',
				'Files found',
				'=========',
			]);

			while (($child = array_shift($children)))
			{
				$output->writeln('|-' . $child->name);
			}

			$output->writeln('');
		}
		catch (Exception\InvalidArgumentException $e)
		{
			throw new \Exception('Oop! Looks like the id is invalid. ' . $e->getMessage());
		}
	}
}
